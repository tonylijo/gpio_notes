== GPIO (General Purpose Input Output) 

The most popular hello world program in embedded systems is blinky which blinks an led via a gpio.

GPIO Ports::
Is a collection fixed number of I/O ports. For example GPIO port A have 32 pins. Each micro controllers may have diffrent amount of I/O pins in each ports 

.Example Gpio Ports
|===
| Atmega | NXP | Stm32

| 8
| 32
| 16
|===

.Gpio Internal
image::{localpath}/images/gpio_1.jpeg["gpio internal",align="center",float="center",pdfwidth=70%]

.Buffer Internal
image::{localpath}/images/buffer.jpeg["buffer internal",align="center",float="center",pdfwidth=70%]

.Buffer Truth Table
|===
| PIN | T1 | T2 | D 

| Driven High
| ON
| OFF
| HIGH

| Driven Low
| OFF
| ON
| LOW
|===

=== GPIO Input Mode with High impedence State (Hi-Z)

High impedence also called high z state is nothing but keeping your pin floting (ie, not connecting it to high or low).

.Internal Pull UP
image::{localpath}/images/internal_pull_up.jpeg["internal pull up resistor",align="center",float="center",pdfwidth=40%]

.Internal Pull down 
image::{localpath}/images/internal_pull_down.jpeg["internal pull down resistor",align="center",float="center",pdfwidth=40%]




<<<
